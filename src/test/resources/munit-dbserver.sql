create table tshirt (
	product_code varchar(200),
	tshirt_size varchar(20),
	description varchar(2000),
	stock_availability integer
);

create table orders (
	order_id varchar(200),
	tshirt_size varchar(20),
	email varchar(2000),
	address varchar(2000),
	city varchar(2000),
	state varchar(2000),
	country varchar(2000),
	postal_code varchar(2000)
);

insert into tshirt values ('9fe413b6-5525-40f0-8949-b2d403d8714d', 'S', 'Small t-shirt', 10);
insert into tshirt values ('0954c3b3-c896-4b17-89e6-5ba53f12b46c', 'M', 'Medium t-shirt', 4);
insert into tshirt values ('187816fc-a293-46be-8ca0-7da84b4cf841', 'L', 'Medium t-shirt', 6);
insert into tshirt values ('187816fc-a293-46be-8ca0-7da84b4cf841', 'XL', 'Medium t-shirt', 0);